# GitLab Terraform RDS example

You can fork this project and by following instructions below, have a GitLab 
deploy and manage your Amazon RDS for you.  

## How to use

#### 1. Make sure you have AWS credentials you are going to use with Amazon RDS.

1. Sign up for [an AWS account](https://docs.aws.amazon.com/IAM/latest/UserGuide/getting-set-up.html) if you don't have one yet.
1. Log in onto the console and create [a new IAM user](https://console.aws.amazon.com/iam/home#/home).
1. Select your newly created user to access its details. Navigate to **Security credentials > Create a new access key**.

   NOTE: **Note:**
   A new **Access key ID** and **Secret access key** pair will be generated. Please take a note of them right away.

#### 2. Set AWS credentials as CI/CD variables in GitLab.

In your GitLab project, go to **Settings > CI / CD**. Set the following as
[environment variables](../variables/README.md#gitlab-cicd-environment-variables)
(see table below):

- Access key ID.
- Secret access key.

| Env. variable name      | Value                  |
|:------------------------|:-----------------------|
| `AWS_ACCESS_KEY_ID`     | Your Access key ID     |
| `AWS_SECRET_ACCESS_KEY` | Your Secret access key |

#### 3. Run the pipeline.

You should see both `validate` and `plan` CI jobs succeded. If you want 
to proceed further and create the database instance, you need to manually start
`apply` job.

## Configuration

You might want to adjust configuration in `main.tf` according to your need. 
Check input values like `region`, `publicly_accessible`, `instance_type`, `replica_scale_max` and `tags`.

For mode details on configuration and available options, see https://github.com/terraform-aws-modules/terraform-aws-rds-aurora 

## FAQ

**1. How I access Terraform state on my local machine**

While doing `terraform init` you need to provide few details on backend config

```bash
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/<YOUR-PROJECT-ID>/terraform/state/<YOUR-STATE-NAME>" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/<YOUR-PROJECT-ID>/terraform/state/<YOUR-STATE-NAME>/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/<YOUR-PROJECT-ID>/terraform/state/<YOUR-STATE-NAME>/lock" \
    -backend-config="username=<YOUR-USERNAME>" \
    -backend-config="password=<YOUR-ACCESS-TOKEN>" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```

**2. What are username and password to connect to the database**

You can find username in `main.tf` file. For password you can run 

```bash
terraform output this_rds_cluster_master_password
```

**3. Where can I find more information on using Terraform with GitLab** 

There is more in GitLab documentation. See [Infrastructure as code with Terraform and GitLab](https://docs.gitlab.com/ee/user/infrastructure/)
